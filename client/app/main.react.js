
import React from 'react'
import ReactDOM from 'react-dom'
import App from './App.jsx'

require('../assets/style.css');

ReactDOM.render(<App />, document.getElementById('app-container'));